# Third-Semester

Collection of the academics books, notes and more that I used as a Electrical Engineering undergraduate in IUT (Islamic University of Technology, Bangladesh).

# Fork it!
If you are from later batch and find this repo somewhat outdated, you are encouraged to fork it and start from there. I hope it will give you a head start. Enjoy.

Sage SixByte;
IUT, EEE'18
.



├── EEE 4301\
│   ├── 81517930_810228229403099_8921732811459657728_o.jpg\
│   ├── AC distribution.pdf\
│   ├── Batch_17\
│   │   ├── AC distribution [8 pg].pdf\
│   │   ├── Alternating Current circuits.pdf\
│   │   ├── corona.pdf\
│   │   ├── Principles of Power System by V. K. Mehta.pdf\
│   │   ├── String efficiency.pdf\
│   │   └── Supply system [6 pg].pdf\
│   ├── Cable fault test.pdf\
│   ├── Ch3@Arther Bergen.pdf\
│   ├── distribution-sag-corona.pdf\
│   ├── Electric Power Systems\
│   │   ├── Electric Power Systems.jpg\
│   │   ├── electric_power_systems.pdf\
│   │   ├── Electric Power Systems.txt\
│   │   ├── Modern Power Systems Analysis- 3rd Edition by D P Kothari & I J Nagrath.pdf\
│   │   └── Power System Analysis - John J. Grainger & William D. Stevenson, Jr - McGraw Hill (Solution Manual)\
│   │       ├── ch10-sol.pdf\
│   │       ├── ch11-sol.pdf\
│   │       ├── ch12soln.pdf\
│   │       ├── ch13soln.pdf\
│   │       ├── ch14soln.pdf\
│   │       ├── ch15soln.pdf\
│   │       ├── ch16soln.pdf\
│   │       ├── Ch1_soln.pdf\
│   │       ├── Ch2_soln.pdf\
│   │       ├── ch3-soln.pdf\
│   │       ├── Ch4_soln.pdf\
│   │       ├── Ch5_soln.pdf\
│   │       ├── ch6-soln.pdf\
│   │       ├── ch7-soln.pdf\
│   │       ├── ch8-soln.pdf\
│   │       ├── ch9-soln.pdf\
│   │       └── Elements of Power System Analysis - Stevenson (Solution).pdf\
│   ├── epdf.pub_power-system-analysis.pdf\
│   ├── Generalised-ABCD-constant-of-line.pdf\
│   ├── Power-Systems_EEE-4301.pdf\
│   ├── Power System Stability And Control by Prabha Kundur.pdf\
│   ├── Principles of Power System (V.K. Mehta & Rohit Mehta)\
│   │   ├── Ch-01.pdf\
│   │   ├── Ch-02.pdf\
│   │   ├── Ch-03.pdf\
│   │   ├── Ch-04.pdf\
│   │   ├── Ch-05.pdf\
│   │   ├── Ch-06.pdf\
│   │   ├── Ch-07.pdf\
│   │   ├── Ch-08.pdf\
│   │   ├── Ch-09.pdf\
│   │   ├── Ch-10.pdf\
│   │   ├── Ch-11.pdf\
│   │   ├── Ch-12.pdf\
│   │   ├── Ch-13.pdf\
│   │   ├── Ch-14.pdf\
│   │   ├── Ch-15.pdf\
│   │   ├── Ch-16.pdf\
│   │   ├── Ch-17.pdf\
│   │   ├── Ch-18.pdf\
│   │   ├── Ch-19.pdf\
│   │   ├── Ch-20.pdf\
│   │   ├── Ch-21.pdf\
│   │   ├── Ch-22.pdf\
│   │   ├── Ch-23.pdf\
│   │   ├── Ch-24.pdf\
│   │   ├── Ch-25.pdf\
│   │   ├── Ch-26.pdf\
│   │   ├── Index.pdf\
│   │   └── Principles Of Electrical Machines_V.K Mehta.pdf\
│   ├── transline.txt\
│   └── Transmission System.pdf\
├── EEE 4303\
│   ├── DriscollCoughlin.pdf\
│   ├── Microelectronic Circuits Sedra Smith 7th Edition [Textbook].pdf\
│   ├── Microelectronics, Circuit Analysis and Design by Donald A. Neamen, 4th edition.pdf\
│   ├── Microelectronics_Circuit_Analysis_and_Design_Donald_Neamen_4th_Solutions.pdf\
│   ├── Sadiku.pdf\
│   ├── Sadiku solve of practice prob\
│   │   ├── Chapt01.pdf\
│   │   ├── Chapt02.pdf\
│   │   ├── Chapt03.pdf\
│   │   ├── Chapt04.pdf\
│   │   ├── Chapt05.pdf\
│   │   ├── Chapt06.pdf\
│   │   ├── Chapt07.pdf\
│   │   ├── Chapt08.pdf\
│   │   ├── Chapt09.pdf\
│   │   ├── Chapt10.pdf\
│   │   ├── Chapt11.pdf\
│   │   ├── Chapt12.pdf\
│   │   ├── Chapt13.pdf\
│   │   ├── Chapt14.pdf\
│   │   ├── Chapt15.pdf\
│   │   ├── Chapt16.pdf\
│   │   ├── Chapt17.pdf\
│   │   ├── Chapt18.pdf\
│   │   ├── Chapt19.pdf\
│   │   └── Fundamentals Of Electric Circuits_Sadiku_3rd Ed.pdf\
│   └── Test-2.pdf\
├── EEE 4305\
│   ├── 1. Stephen J. Chapman.pdf\
│   ├── 2. B.L. Theraja.pdf\
│   ├── 3. Bhag S. Guru.pdf\
│   ├── 6. Duplex lap winding.docx\
│   ├── 6. Winding and Construction.pdf\
│   ├── 6. Winding & Construction.pdf\
│   ├── 7. Ring Winding.png\
│   ├── chp-28-part-1-theraja.pdf\
│   ├── chp-28-part-2-theraja.pdf\
│   ├── Eee-4305-chp-28-part-2-theraja.pdf\
│   ├── electric-machinery-fundamentals-4th-edition-solution-manual-chapman.pdf\
│   ├── Electric Machinery.pdf\
│   ├── Mehta_principles of electrical machines(mehta).pdf\
│   ├── Questions\
│   │   ├── IMG_0201.JPG\
│   │   ├── IMG_0202.JPG\
│   │   ├── IMG_0203.JPG\
│   │   ├── IMG_0204.JPG\
│   │   ├── IMG_0205.JPG\
│   │   ├── IMG_0206.JPG\
│   │   ├── IMG_0207.JPG\
│   │   ├── IMG_0208.JPG\
│   │   ├── IMG_0209.JPG\
│   │   └── IMG_0210.JPG\
│   └── Wildi_Electrical Machines, Drives, and Power Systems 5E.pdf\
├── EEE 4307\
│   ├── 1.DIGITAL DESIGN 3rd edition - M. Morris Mano.pdf\
│   ├── 1.M. Morris R. Mano, Charles R. Kime, Tom Martin-Logic and computer design fundamentals-Prentice Hall (2015).pdf\
│   ├── 2.Thomas L. Floyd-Digital Fundamentals-Prentice Hall (2014).pdf\
│   ├── 3.Fundamentals of Logic Design CH Roth 6th edition.pdf\
│   ├── 4.Anil Kumar Maini Digital Electronics Principles, Devices and Applications.pdf\
│   ├── 5.Digital electronics a practical approach  with VHDL   by William Kleitz.pdf\
│   ├── Assignment 2.pdf\
│   ├── EEE-4307_Counters_Registers_MemoryUnit.pdf\
│   ├── Floyd_Digital.Fundamentals.10th.Edition.Solutions.pdf\
│   └── Solutions_Manual_Digital__Fundamentals__(9th ed)-_Thomas__L__Floyd.pdf\
├── MATH\
│   ├── 9th Howard_Anton_Solution_Manual.pdf\
│   ├── elementary_linear_algebra_10th_edition_2.pdf\
│   ├── fft.jpg\
│   ├── Fourier-series-1.doc\
│   ├── Fourier-series-1.pdf\
│   ├── Fourier-sine-cosine-series.pdf\
│   ├── FourierTransformNote.pdf\
│   ├── Inverse-Laplace20.pdf\
│   ├── Laplace application-20.ppt.pdf\
│   ├── Laplace.pdf\
│   ├── LINEAR ALGEBRA  - Anton - 11th Ed.pdf\
│   ├── onlineclass1\
│   │   ├── document2.pdf\
│   │   ├── document3.pdf\
│   │   ├── document4.pdf\
│   │   ├── document.pdf\
│   │   └── outline.pdf\
│   ├── schaums-outline-of-laplace-transforms.pdf\
│   ├── self-paced-trigonometry.pdf\
│   ├── sine integral.pdf\
│   ├── slides7-17.pdf\
│   └── TrigonometryHandbook.pdf\
├── MCE 4391\
│   ├── 1 - Biomechanics.pdf\
│   ├── 1 - Mechanics.pdf\
│   ├── 2 - Turbines.pdf\
│   ├── 7_RAC-Basics-and-Applications.pdf\
│   ├── 8_Refrigeration-Methods.pdf\
│   ├── 9_Refrigerant.pdf\
│   ├── FLUID MECHANICS\
│   │   ├── Lecture 2 - Turbines.pdf\
│   │   ├── Lecture 3 - Turbines.pdf\
│   │   ├── MCE Math.pdf\
│   │   ├── Merle C. Potter, David C. Wiggert, Bassem H. Ramadan - Mechanics of Fluids , Fourth Edition.pdf\
│   │   └── Yunus Cengel, John Cimbala - Fluid Mechanics Fundamentals and Applications.pdf\
│   ├── IC-Engine-Lecture-II.pdf\
│   ├── MCE 4391\
│   │   ├── 10_Refrigeration equipment.pptx\
│   │   ├── 11_Psychometry.pptx\
│   │   ├── 12_Study of air conditioning system.pptx\
│   │   ├── 1_Study of Fuels.pptx\
│   │   ├── 2_Basic Thermodynamics.pptx\
│   │   ├── 3_1st_2nd Laws of Thermodynamics.pptx\
│   │   ├── 4_Problem_Thermodynamics Cycles.pptx\
│   │   ├── 5_Steam Generator_Boiler.pptx\
│   │   ├── 6_Steam Turbine.pptx\
│   │   ├── 7_RAC Basics and Applications.pptx\
│   │   ├── 8_Refrigeration Methods.pptx\
│   │   ├── 9_Refrigerant.pptx\
│   │   └── Lecture 1 - Biomechanics.pdf\
│   ├── Pump II.pdf\
│   ├── Pump I.pdf\
│   └── THERMODYNAMICS\
│       ├── 2_Basic Thermodynamics_Abedin.pptx\
│       ├── 3_1st_2nd Laws of Thermodynamics(1).pptx\
│       ├── 4_Problem_Thermodynamics Cycles.pptx\
│       ├── 5_Steam Generator_Boiler.pptx\
│       ├── 6_Steam Turbine.pptx\
│       └── Study of Fuels_Abedin.pptx\
└── README.md

16 directories, 181 files
